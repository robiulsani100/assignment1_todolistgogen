package application

import (
	"tugasgogen/domain_todocore/controller/restapi"
	"tugasgogen/domain_todocore/gateway/withgorm"
	"tugasgogen/domain_todocore/usecase/getalltodo"
	"tugasgogen/domain_todocore/usecase/runtodocheck"
	"tugasgogen/domain_todocore/usecase/runtodocreate"
	"tugasgogen/shared/config"
	"tugasgogen/shared/gogen"
	"tugasgogen/shared/infrastructure/logger"
	"tugasgogen/shared/infrastructure/token"
)

type todoapp struct{}

func NewTodoapp() gogen.Runner {
	return &todoapp{}
}

func (todoapp) Run() error {

	const appName = "todoapp"

	cfg := config.ReadConfig()

	appData := gogen.NewApplicationData(appName)

	log := logger.NewSimpleJSONLogger(appData)

	jwtToken := token.NewJWTToken(cfg.JWTSecretKey)

	datasource := withgorm.NewGateway(log, appData, cfg)

	primaryDriver := restapi.NewController(appData, log, cfg, jwtToken)

	primaryDriver.AddUsecase(
		//
		getalltodo.NewUsecase(datasource),
		runtodocheck.NewUsecase(datasource),
		runtodocreate.NewUsecase(datasource),
	)

	primaryDriver.RegisterRouter()

	primaryDriver.Start()

	return nil
}
