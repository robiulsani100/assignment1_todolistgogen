package runtodocheck

import (
	"tugasgogen/domain_todocore/model/entity"
	"tugasgogen/domain_todocore/model/vo"
	"tugasgogen/shared/gogen"
)

type Inport = gogen.Inport[InportRequest, InportResponse]

type InportRequest struct {
	TodoID vo.TodoID
}

type InportResponse struct {
	Todo *entity.Todo
}
