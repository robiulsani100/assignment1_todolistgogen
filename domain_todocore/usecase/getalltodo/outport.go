package getalltodo

import "tugasgogen/domain_todocore/model/repository"

type Outport interface {
	repository.FindAllTodoRepo
}
