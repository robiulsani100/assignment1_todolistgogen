package runtodocreate

import (
	"tugasgogen/domain_todocore/model/entity"
	"tugasgogen/shared/gogen"
)

type Inport = gogen.Inport[InportRequest, InportResponse]

type InportRequest struct {
	entity.TodoCreateRequest
}

type InportResponse struct {
	Todo *entity.Todo
}
