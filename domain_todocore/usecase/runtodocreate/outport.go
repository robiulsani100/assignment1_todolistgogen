package runtodocreate

import "tugasgogen/domain_todocore/model/repository"

type Outport interface {
	repository.SaveTodoRepo
}
